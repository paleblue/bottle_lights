EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x05 J?
U 1 1 5F813F54
P 3050 1850
F 0 "J?" H 3000 2150 50  0000 L CNN
F 1 "Conn_01x05" V 3150 1650 50  0000 L CNN
F 2 "" H 3050 1850 50  0001 C CNN
F 3 "~" H 3050 1850 50  0001 C CNN
	1    3050 1850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F813F5A
P 2850 1850
F 0 "#PWR?" H 2850 1600 50  0001 C CNN
F 1 "GND" V 2855 1677 50  0000 C CNN
F 2 "" H 2850 1850 50  0001 C CNN
F 3 "" H 2850 1850 50  0001 C CNN
	1    2850 1850
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5F813F60
P 2700 4700
F 0 "J?" H 2780 4692 50  0000 L CNN
F 1 "Conn_01x02" H 2780 4601 50  0000 L CNN
F 2 "" H 2700 4700 50  0001 C CNN
F 3 "~" H 2700 4700 50  0001 C CNN
	1    2700 4700
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5F813F66
P 2400 5500
F 0 "J?" H 2480 5492 50  0000 L CNN
F 1 "Conn_01x02" H 2480 5401 50  0000 L CNN
F 2 "" H 2400 5500 50  0001 C CNN
F 3 "~" H 2400 5500 50  0001 C CNN
	1    2400 5500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5F813F6C
P 2400 6000
F 0 "J?" H 2480 5992 50  0000 L CNN
F 1 "Conn_01x02" H 2480 5901 50  0000 L CNN
F 2 "" H 2400 6000 50  0001 C CNN
F 3 "~" H 2400 6000 50  0001 C CNN
	1    2400 6000
	1    0    0    -1  
$EndComp
Text Notes 3200 4600 2    50   ~ 0
incoming power - castellation
Text Notes 2650 5400 2    50   ~ 0
pair 1 connector
Text Notes 2700 5900 2    50   ~ 0
pair 2 connector
$Comp
L Device:C C?
U 1 1 5F813F75
P 2200 4850
F 0 "C?" H 2315 4896 50  0000 L CNN
F 1 "10nF" H 2315 4805 50  0000 L CNN
F 2 "" H 2238 4700 50  0001 C CNN
F 3 "~" H 2200 4850 50  0001 C CNN
	1    2200 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 4700 2200 4700
Wire Wire Line
	2500 4800 2500 5000
Wire Wire Line
	2200 5000 2500 5000
Wire Wire Line
	2200 5000 2050 5000
Connection ~ 2200 5000
Wire Wire Line
	2200 4700 2050 4700
Connection ~ 2200 4700
$Comp
L power:GND #PWR?
U 1 1 5F813F82
P 2050 5000
F 0 "#PWR?" H 2050 4750 50  0001 C CNN
F 1 "GND" V 2055 4827 50  0000 C CNN
F 2 "" H 2050 5000 50  0001 C CNN
F 3 "" H 2050 5000 50  0001 C CNN
	1    2050 5000
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 5F813F88
P 2500 1500
F 0 "#PWR?" H 2500 1350 50  0001 C CNN
F 1 "VCC" H 2515 1673 50  0000 C CNN
F 2 "" H 2500 1500 50  0001 C CNN
F 3 "" H 2500 1500 50  0001 C CNN
	1    2500 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 1500 2500 1750
Connection ~ 2500 1750
Wire Wire Line
	2500 1750 2500 2450
$Comp
L power:VCC #PWR?
U 1 1 5F813F91
P 2050 4700
F 0 "#PWR?" H 2050 4550 50  0001 C CNN
F 1 "VCC" V 2065 4827 50  0000 L CNN
F 2 "" H 2050 4700 50  0001 C CNN
F 3 "" H 2050 4700 50  0001 C CNN
	1    2050 4700
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F813F97
P 2500 4050
F 0 "#PWR?" H 2500 3800 50  0001 C CNN
F 1 "GND" H 2505 3877 50  0000 C CNN
F 2 "" H 2500 4050 50  0001 C CNN
F 3 "" H 2500 4050 50  0001 C CNN
	1    2500 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 3650 2500 4050
$Comp
L BottleLights-rescue:PIC16F1574-ISL_0-MCU_Microchip_PIC16 U?
U 1 1 5F813F9E
P 2500 3050
F 0 "U?" H 2475 3828 50  0000 C CNN
F 1 "PIC16F1574-ISL_0" H 2475 3737 50  0000 C CNN
F 2 "" H 2500 3050 50  0001 C CNN
F 3 "" H 2500 3050 50  0001 C CNN
	1    2500 3050
	1    0    0    -1  
$EndComp
NoConn ~ 1650 2950
NoConn ~ 1650 3150
NoConn ~ 1650 3250
Wire Wire Line
	1550 2750 1650 2750
Wire Wire Line
	1300 3050 1300 1650
Wire Wire Line
	1300 3050 1650 3050
Wire Wire Line
	1550 2750 1550 1950
Wire Wire Line
	1400 2850 1400 2050
Wire Wire Line
	1400 2850 1650 2850
NoConn ~ 3300 2750
NoConn ~ 3300 2850
NoConn ~ 3300 2950
NoConn ~ 3300 3050
NoConn ~ 3300 3150
NoConn ~ 3300 3250
Wire Wire Line
	1300 1650 2850 1650
Wire Wire Line
	1400 2050 2850 2050
Wire Wire Line
	1550 1950 2850 1950
Wire Wire Line
	2500 1750 2850 1750
$EndSCHEMATC
