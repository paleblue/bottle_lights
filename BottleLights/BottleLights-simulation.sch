EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Bottle Light Controller"
Date ""
Rev "1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 5000 1200 0    50   ~ 0
pin9
Text Label 5000 1500 0    50   ~ 0
pin7
Text Label 5000 1350 0    50   ~ 0
pin8
Text Label 5000 1650 0    50   ~ 0
pin6
Text Label 3450 1400 2    50   ~ 0
pin5
$Comp
L Device:Battery_Cell V1
U 1 1 5F7CAD01
P 4150 1450
AR Path="/5F7CAD01" Ref="V1"  Part="1" 
AR Path="/5F7559D3/5F7CAD01" Ref="V?"  Part="1" 
F 0 "V1" H 4268 1546 50  0000 L CNN
F 1 "DC 5" H 4268 1455 50  0000 L CNN
F 2 "" V 4150 1510 50  0001 C CNN
F 3 "~" V 4150 1510 50  0001 C CNN
	1    4150 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 1200 5000 1200
Wire Wire Line
	4650 1350 5000 1350
Wire Wire Line
	4650 1200 4650 1350
Connection ~ 4650 1350
Wire Wire Line
	4650 1500 5000 1500
Wire Wire Line
	4650 1650 4650 1500
Wire Wire Line
	4650 1650 5000 1650
Connection ~ 4650 1500
$Comp
L power:GND #PWR0101
U 1 1 5F7CAD0F
P 4150 1700
AR Path="/5F7CAD0F" Ref="#PWR0101"  Part="1" 
AR Path="/5F7559D3/5F7CAD0F" Ref="#PWR?"  Part="1" 
F 0 "#PWR0101" H 4150 1450 50  0001 C CNN
F 1 "GND" H 4155 1527 50  0000 C CNN
F 2 "" H 4150 1700 50  0001 C CNN
F 3 "" H 4150 1700 50  0001 C CNN
	1    4150 1700
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0102
U 1 1 5F7CAD15
P 4150 1150
AR Path="/5F7CAD15" Ref="#PWR0102"  Part="1" 
AR Path="/5F7559D3/5F7CAD15" Ref="#PWR?"  Part="1" 
F 0 "#PWR0102" H 4150 1000 50  0001 C CNN
F 1 "VCC" H 4165 1323 50  0000 C CNN
F 2 "" H 4150 1150 50  0001 C CNN
F 3 "" H 4150 1150 50  0001 C CNN
	1    4150 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 1250 4150 1200
Wire Wire Line
	4150 1550 4150 1650
Wire Wire Line
	4250 1650 4150 1650
Connection ~ 4150 1650
Wire Wire Line
	4150 1650 4150 1700
Wire Wire Line
	4050 1500 4050 1650
Wire Wire Line
	4050 1650 4150 1650
Wire Wire Line
	4150 1200 4050 1200
Wire Wire Line
	4050 1200 4050 1300
Connection ~ 4150 1200
Wire Wire Line
	4150 1200 4150 1150
Wire Wire Line
	4250 1300 4250 1200
Wire Wire Line
	4250 1200 4150 1200
Wire Wire Line
	4650 1350 4650 1400
Wire Wire Line
	3450 1400 3650 1400
Wire Wire Line
	3650 1400 3650 1500
Wire Wire Line
	4650 1400 4500 1400
Wire Wire Line
	4500 1400 4500 1300
Connection ~ 4650 1400
Wire Wire Line
	4650 1400 4650 1500
$Comp
L Device:LED D?
U 1 1 5F7DB836
P 8900 3550
AR Path="/5F7559D3/5F7DB836" Ref="D?"  Part="1" 
AR Path="/5F7DB836" Ref="D2"  Part="1" 
F 0 "D2" V 8845 3628 50  0000 L CNN
F 1 "LED" V 8936 3628 50  0000 L CNN
F 2 "" H 8900 3550 50  0001 C CNN
F 3 "~" H 8900 3550 50  0001 C CNN
F 4 "D" H 8900 3550 50  0001 C CNN "Spice_Primitive"
F 5 "led" H 8900 3550 50  0001 C CNN "Spice_Model"
F 6 "Y" H 8900 3550 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "D:\\Projects\\_electronics\\bottle_lights\\led.lib" H 8900 3550 50  0001 C CNN "Spice_Lib_File"
	1    8900 3550
	0    1    1    0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5F7DB840
P 9250 3550
AR Path="/5F7559D3/5F7DB840" Ref="D?"  Part="1" 
AR Path="/5F7DB840" Ref="D4"  Part="1" 
F 0 "D4" V 9288 3433 50  0000 R CNN
F 1 "LED" V 9197 3433 50  0000 R CNN
F 2 "" H 9250 3550 50  0001 C CNN
F 3 "~" H 9250 3550 50  0001 C CNN
F 4 "D" H 9250 3550 50  0001 C CNN "Spice_Primitive"
F 5 "led" H 9250 3550 50  0001 C CNN "Spice_Model"
F 6 "Y" H 9250 3550 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "D:\\Projects\\_electronics\\bottle_lights\\led.lib" H 9250 3550 50  0001 C CNN "Spice_Lib_File"
	1    9250 3550
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5F7DB84A
P 8900 2350
AR Path="/5F7559D3/5F7DB84A" Ref="D?"  Part="1" 
AR Path="/5F7DB84A" Ref="D1"  Part="1" 
F 0 "D1" V 8845 2428 50  0000 L CNN
F 1 "LED" V 8936 2428 50  0000 L CNN
F 2 "" H 8900 2350 50  0001 C CNN
F 3 "~" H 8900 2350 50  0001 C CNN
F 4 "D" H 8900 2350 50  0001 C CNN "Spice_Primitive"
F 5 "led" H 8900 2350 50  0001 C CNN "Spice_Model"
F 6 "Y" H 8900 2350 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "D:\\Projects\\_electronics\\bottle_lights\\led.lib" H 8900 2350 50  0001 C CNN "Spice_Lib_File"
	1    8900 2350
	0    1    1    0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5F7DB854
P 9250 2350
AR Path="/5F7559D3/5F7DB854" Ref="D?"  Part="1" 
AR Path="/5F7DB854" Ref="D3"  Part="1" 
F 0 "D3" V 9288 2233 50  0000 R CNN
F 1 "LED" V 9197 2233 50  0000 R CNN
F 2 "" H 9250 2350 50  0001 C CNN
F 3 "~" H 9250 2350 50  0001 C CNN
F 4 "D" H 9250 2350 50  0001 C CNN "Spice_Primitive"
F 5 "led" H 9250 2350 50  0001 C CNN "Spice_Model"
F 6 "Y" H 9250 2350 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "D:\\Projects\\_electronics\\bottle_lights\\led.lib" H 9250 2350 50  0001 C CNN "Spice_Lib_File"
	1    9250 2350
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F7DB866
P 4400 5050
AR Path="/5F7559D3/5F7DB866" Ref="#PWR?"  Part="1" 
AR Path="/5F7DB866" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 4400 4800 50  0001 C CNN
F 1 "GND" H 4405 4877 50  0000 C CNN
F 2 "" H 4400 5050 50  0001 C CNN
F 3 "" H 4400 5050 50  0001 C CNN
	1    4400 5050
	0    1    1    0   
$EndComp
$Comp
L UMZ1NTR:UMZ1NTR U?
U 1 1 5F7DB870
P 4400 2800
AR Path="/5F7559D3/5F7DB870" Ref="U?"  Part="1" 
AR Path="/5F7DB870" Ref="U1"  Part="1" 
F 0 "U1" H 5450 3187 60  0000 C CNN
F 1 "UMZ1NTR" H 5450 3081 60  0000 C CNN
F 2 "TRANS_UM6J1_ROM" H 5450 3040 60  0001 C CNN
F 3 "" H 4400 2800 60  0000 C CNN
F 4 "X" H 4400 2800 50  0001 C CNN "Spice_Primitive"
F 5 "UMZ1N" H 4400 2800 50  0001 C CNN "Spice_Model"
F 6 "Y" H 4400 2800 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "D:\\Projects\\_electronics\\bottle_lights\\umz1n.lib" H 4400 2800 50  0001 C CNN "Spice_Lib_File"
	1    4400 2800
	1    0    0    -1  
$EndComp
$Comp
L UMZ1NTR:UMZ1NTR U?
U 1 1 5F7DB87A
P 4400 3800
AR Path="/5F7559D3/5F7DB87A" Ref="U?"  Part="1" 
AR Path="/5F7DB87A" Ref="U2"  Part="1" 
F 0 "U2" H 5450 4187 60  0000 C CNN
F 1 "UMZ1NTR" H 5450 4081 60  0000 C CNN
F 2 "TRANS_UM6J1_ROM" H 5450 4040 60  0001 C CNN
F 3 "" H 4400 3800 60  0000 C CNN
F 4 "X" H 4400 3800 50  0001 C CNN "Spice_Primitive"
F 5 "UMZ1N" H 4400 3800 50  0001 C CNN "Spice_Model"
F 6 "Y" H 4400 3800 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "D:\\Projects\\_electronics\\bottle_lights\\umz1n.lib" H 4400 3800 50  0001 C CNN "Spice_Lib_File"
	1    4400 3800
	1    0    0    -1  
$EndComp
$Comp
L UMZ1NTR:UMZ1NTR U?
U 1 1 5F7DB884
P 4400 4850
AR Path="/5F7559D3/5F7DB884" Ref="U?"  Part="1" 
AR Path="/5F7DB884" Ref="U3"  Part="1" 
F 0 "U3" H 5450 5237 60  0000 C CNN
F 1 "UMZ1NTR" H 5450 5131 60  0000 C CNN
F 2 "TRANS_UM6J1_ROM" H 5450 5090 60  0001 C CNN
F 3 "" H 4400 4850 60  0000 C CNN
F 4 "X" H 4400 4850 50  0001 C CNN "Spice_Primitive"
F 5 "UMZ1N" H 4400 4850 50  0001 C CNN "Spice_Model"
F 6 "Y" H 4400 4850 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "D:\\Projects\\_electronics\\bottle_lights\\umz1n.lib" H 4400 4850 50  0001 C CNN "Spice_Lib_File"
	1    4400 4850
	1    0    0    -1  
$EndComp
$Comp
L UMZ1NTR:UMZ1NTR U?
U 1 1 5F7DB88E
P 4400 5850
AR Path="/5F7559D3/5F7DB88E" Ref="U?"  Part="1" 
AR Path="/5F7DB88E" Ref="U4"  Part="1" 
F 0 "U4" H 5450 6237 60  0000 C CNN
F 1 "UMZ1NTR" H 5450 6131 60  0000 C CNN
F 2 "TRANS_UM6J1_ROM" H 5450 6090 60  0001 C CNN
F 3 "" H 4400 5850 60  0000 C CNN
F 4 "X" H 4400 5850 50  0001 C CNN "Spice_Primitive"
F 5 "UMZ1N" H 4400 5850 50  0001 C CNN "Spice_Model"
F 6 "Y" H 4400 5850 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "D:\\Projects\\_electronics\\bottle_lights\\umz1n.lib" H 4400 5850 50  0001 C CNN "Spice_Lib_File"
	1    4400 5850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F7DB894
P 3950 3900
AR Path="/5F7559D3/5F7DB894" Ref="R?"  Part="1" 
AR Path="/5F7DB894" Ref="R2"  Part="1" 
F 0 "R2" H 3880 3854 50  0000 R CNN
F 1 "10k" H 3880 3945 50  0000 R CNN
F 2 "" V 3880 3900 50  0001 C CNN
F 3 "~" H 3950 3900 50  0001 C CNN
	1    3950 3900
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5F7DB89A
P 4250 2800
AR Path="/5F7559D3/5F7DB89A" Ref="R?"  Part="1" 
AR Path="/5F7DB89A" Ref="R5"  Part="1" 
F 0 "R5" H 4180 2754 50  0000 R CNN
F 1 "47R" H 4180 2845 50  0000 R CNN
F 2 "" V 4180 2800 50  0001 C CNN
F 3 "~" H 4250 2800 50  0001 C CNN
	1    4250 2800
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5F7DB8A0
P 3950 5950
AR Path="/5F7559D3/5F7DB8A0" Ref="R?"  Part="1" 
AR Path="/5F7DB8A0" Ref="R4"  Part="1" 
F 0 "R4" H 3880 5904 50  0000 R CNN
F 1 "22k" H 3880 5995 50  0000 R CNN
F 2 "" V 3880 5950 50  0001 C CNN
F 3 "~" H 3950 5950 50  0001 C CNN
	1    3950 5950
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5F7DB8A6
P 6850 4950
AR Path="/5F7559D3/5F7DB8A6" Ref="R?"  Part="1" 
AR Path="/5F7DB8A6" Ref="R9"  Part="1" 
F 0 "R9" H 6780 4904 50  0000 R CNN
F 1 "100R" H 6780 4995 50  0000 R CNN
F 2 "" V 6780 4950 50  0001 C CNN
F 3 "~" H 6850 4950 50  0001 C CNN
	1    6850 4950
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5F7DB8AC
P 6850 5950
AR Path="/5F7559D3/5F7DB8AC" Ref="R?"  Part="1" 
AR Path="/5F7DB8AC" Ref="R10"  Part="1" 
F 0 "R10" H 6780 5904 50  0000 R CNN
F 1 "100R" H 6780 5995 50  0000 R CNN
F 2 "" V 6780 5950 50  0001 C CNN
F 3 "~" H 6850 5950 50  0001 C CNN
	1    6850 5950
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5F7DB8B2
P 3950 4950
AR Path="/5F7559D3/5F7DB8B2" Ref="R?"  Part="1" 
AR Path="/5F7DB8B2" Ref="R3"  Part="1" 
F 0 "R3" H 3880 4904 50  0000 R CNN
F 1 "22k" H 3880 4995 50  0000 R CNN
F 2 "" V 3880 4950 50  0001 C CNN
F 3 "~" H 3950 4950 50  0001 C CNN
	1    3950 4950
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5F7DB8B8
P 6900 2900
AR Path="/5F7559D3/5F7DB8B8" Ref="R?"  Part="1" 
AR Path="/5F7DB8B8" Ref="R11"  Part="1" 
F 0 "R11" H 6830 2854 50  0000 R CNN
F 1 "22k" H 6830 2945 50  0000 R CNN
F 2 "" V 6830 2900 50  0001 C CNN
F 3 "~" H 6900 2900 50  0001 C CNN
	1    6900 2900
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5F7DB8BE
P 6900 3900
AR Path="/5F7559D3/5F7DB8BE" Ref="R?"  Part="1" 
AR Path="/5F7DB8BE" Ref="R12"  Part="1" 
F 0 "R12" H 6830 3854 50  0000 R CNN
F 1 "22k" H 6830 3945 50  0000 R CNN
F 2 "" V 6830 3900 50  0001 C CNN
F 3 "~" H 6900 3900 50  0001 C CNN
	1    6900 3900
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F7DB8D6
P 4400 6050
AR Path="/5F7559D3/5F7DB8D6" Ref="#PWR?"  Part="1" 
AR Path="/5F7DB8D6" Ref="#PWR0108"  Part="1" 
F 0 "#PWR0108" H 4400 5800 50  0001 C CNN
F 1 "GND" H 4405 5877 50  0000 C CNN
F 2 "" H 4400 6050 50  0001 C CNN
F 3 "" H 4400 6050 50  0001 C CNN
	1    4400 6050
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F7DB8DC
P 4400 5850
AR Path="/5F7559D3/5F7DB8DC" Ref="#PWR?"  Part="1" 
AR Path="/5F7DB8DC" Ref="#PWR0109"  Part="1" 
F 0 "#PWR0109" H 4400 5600 50  0001 C CNN
F 1 "GND" H 4405 5677 50  0000 C CNN
F 2 "" H 4400 5850 50  0001 C CNN
F 3 "" H 4400 5850 50  0001 C CNN
	1    4400 5850
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F7DB8E2
P 4400 4850
AR Path="/5F7559D3/5F7DB8E2" Ref="#PWR?"  Part="1" 
AR Path="/5F7DB8E2" Ref="#PWR0110"  Part="1" 
F 0 "#PWR0110" H 4400 4600 50  0001 C CNN
F 1 "GND" H 4405 4677 50  0000 C CNN
F 2 "" H 4400 4850 50  0001 C CNN
F 3 "" H 4400 4850 50  0001 C CNN
	1    4400 4850
	0    1    1    0   
$EndComp
Wire Wire Line
	4100 5950 4400 5950
Text Label 3700 5950 2    50   ~ 0
pin5
Wire Wire Line
	4100 4950 4400 4950
Wire Wire Line
	6500 4950 6700 4950
Wire Wire Line
	6500 5950 6700 5950
Text Label 7100 4950 0    50   ~ 0
pin5
Text Label 7100 5950 0    50   ~ 0
pin5
Wire Wire Line
	4100 3900 4400 3900
Wire Wire Line
	4100 2900 4400 2900
Wire Wire Line
	6500 3900 6750 3900
Wire Wire Line
	6500 2900 6750 2900
Text Label 3700 2900 2    50   ~ 0
pin6
Text Label 3700 3900 2    50   ~ 0
pin7
Text Label 7150 3900 0    50   ~ 0
pin8
Text Label 7150 2900 0    50   ~ 0
pin9
Wire Wire Line
	8900 2200 9250 2200
Connection ~ 8900 2200
Wire Wire Line
	9300 2250 9250 2250
Wire Wire Line
	9250 2250 9250 2200
Connection ~ 9250 2200
Wire Wire Line
	9250 2500 8900 2500
Connection ~ 8900 2500
Wire Wire Line
	9250 3400 8900 3400
Connection ~ 8900 3400
Wire Wire Line
	9250 3700 8900 3700
Connection ~ 8900 3700
$Comp
L Device:R R?
U 1 1 5F7DB902
P 4250 3000
AR Path="/5F7559D3/5F7DB902" Ref="R?"  Part="1" 
AR Path="/5F7DB902" Ref="R6"  Part="1" 
F 0 "R6" H 4180 2954 50  0000 R CNN
F 1 "47R" H 4180 3045 50  0000 R CNN
F 2 "" V 4180 3000 50  0001 C CNN
F 3 "~" H 4250 3000 50  0001 C CNN
	1    4250 3000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4100 2800 4100 2200
Wire Wire Line
	4100 2200 7400 2200
Wire Wire Line
	4100 3000 4100 3300
Wire Wire Line
	4100 3300 7550 3300
Wire Wire Line
	7550 3300 7550 2500
Wire Wire Line
	7550 2500 8200 2500
Wire Wire Line
	8000 4300 8000 3700
Wire Wire Line
	8000 3700 8200 3700
Wire Wire Line
	8000 4300 8000 6050
Wire Wire Line
	8000 6050 6500 6050
Connection ~ 8000 4300
Connection ~ 7550 3300
Wire Wire Line
	7550 5050 6500 5050
Wire Wire Line
	7550 3300 7550 5050
Wire Wire Line
	7400 2200 7400 4850
Wire Wire Line
	7400 4850 6500 4850
Connection ~ 7400 2200
Wire Wire Line
	7400 2200 8200 2200
Wire Wire Line
	7850 3400 7850 5850
Wire Wire Line
	7850 5850 6500 5850
Connection ~ 7850 3400
Wire Wire Line
	7850 3400 8200 3400
Text Label 3700 4950 2    50   ~ 0
pin5
Wire Wire Line
	7050 2900 7150 2900
Wire Wire Line
	7050 3900 7150 3900
Wire Wire Line
	7000 4950 7100 4950
Wire Wire Line
	7000 5950 7100 5950
Wire Wire Line
	3700 5950 3800 5950
Wire Wire Line
	3700 4950 3800 4950
Wire Wire Line
	3700 3900 3800 3900
Wire Wire Line
	3700 2900 3800 2900
$Comp
L Device:R R?
U 1 1 5F7DB928
P 4250 3800
AR Path="/5F7559D3/5F7DB928" Ref="R?"  Part="1" 
AR Path="/5F7DB928" Ref="R7"  Part="1" 
F 0 "R7" H 4180 3754 50  0000 R CNN
F 1 "47R" H 4180 3845 50  0000 R CNN
F 2 "" V 4180 3800 50  0001 C CNN
F 3 "~" H 4250 3800 50  0001 C CNN
	1    4250 3800
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5F7DB92E
P 4250 4000
AR Path="/5F7559D3/5F7DB92E" Ref="R?"  Part="1" 
AR Path="/5F7DB92E" Ref="R8"  Part="1" 
F 0 "R8" H 4180 3954 50  0000 R CNN
F 1 "47R" H 4180 4045 50  0000 R CNN
F 2 "" V 4180 4000 50  0001 C CNN
F 3 "~" H 4250 4000 50  0001 C CNN
	1    4250 4000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4050 4300 4050 4000
Wire Wire Line
	4050 4000 4100 4000
Wire Wire Line
	4050 4300 8000 4300
Wire Wire Line
	4050 3400 4050 3800
Wire Wire Line
	4050 3800 4100 3800
Wire Wire Line
	4050 3400 7850 3400
$Comp
L Connector_Generic:Conn_01x05 J4
U 1 1 5F81D041
P 2650 1400
AR Path="/5F81D041" Ref="J4"  Part="1" 
AR Path="/5F808E80/5F81D041" Ref="J?"  Part="1" 
F 0 "J4" H 2600 1700 50  0000 L CNN
F 1 "Conn_01x05" V 2750 1200 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 2650 1400 50  0001 C CNN
F 3 "~" H 2650 1400 50  0001 C CNN
F 4 "J" H 2650 1400 50  0001 C CNN "Spice_Primitive"
F 5 "Conn_01x05" H 2650 1400 50  0001 C CNN "Spice_Model"
F 6 "N" H 2650 1400 50  0001 C CNN "Spice_Netlist_Enabled"
	1    2650 1400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 5F81D047
P 2450 1400
AR Path="/5F81D047" Ref="#PWR0111"  Part="1" 
AR Path="/5F808E80/5F81D047" Ref="#PWR?"  Part="1" 
F 0 "#PWR0111" H 2450 1150 50  0001 C CNN
F 1 "GND" V 2455 1227 50  0000 C CNN
F 2 "" H 2450 1400 50  0001 C CNN
F 3 "" H 2450 1400 50  0001 C CNN
	1    2450 1400
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 5F81D04D
P 2300 4250
AR Path="/5F81D04D" Ref="J3"  Part="1" 
AR Path="/5F808E80/5F81D04D" Ref="J?"  Part="1" 
F 0 "J3" H 2380 4242 50  0000 L CNN
F 1 "Conn_01x02" H 2380 4151 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 4250 50  0001 C CNN
F 3 "~" H 2300 4250 50  0001 C CNN
F 4 "J" H 2300 4250 50  0001 C CNN "Spice_Primitive"
F 5 "Conn_01x02" H 2300 4250 50  0001 C CNN "Spice_Model"
F 6 "N" H 2300 4250 50  0001 C CNN "Spice_Netlist_Enabled"
	1    2300 4250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5F81D053
P 2000 5050
AR Path="/5F81D053" Ref="J1"  Part="1" 
AR Path="/5F808E80/5F81D053" Ref="J?"  Part="1" 
F 0 "J1" H 2080 5042 50  0000 L CNN
F 1 "Conn_01x02" H 2080 4951 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2000 5050 50  0001 C CNN
F 3 "~" H 2000 5050 50  0001 C CNN
F 4 "J" H 2000 5050 50  0001 C CNN "Spice_Primitive"
F 5 "Conn_01x02" H 2000 5050 50  0001 C CNN "Spice_Model"
F 6 "N" H 2000 5050 50  0001 C CNN "Spice_Netlist_Enabled"
	1    2000 5050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 5F81D059
P 2000 5550
AR Path="/5F81D059" Ref="J2"  Part="1" 
AR Path="/5F808E80/5F81D059" Ref="J?"  Part="1" 
F 0 "J2" H 2080 5542 50  0000 L CNN
F 1 "Conn_01x02" H 2080 5451 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2000 5550 50  0001 C CNN
F 3 "~" H 2000 5550 50  0001 C CNN
F 4 "J" H 2000 5550 50  0001 C CNN "Spice_Primitive"
F 5 "Conn_01x02" H 2000 5550 50  0001 C CNN "Spice_Model"
F 6 "N" H 2000 5550 50  0001 C CNN "Spice_Netlist_Enabled"
	1    2000 5550
	1    0    0    -1  
$EndComp
Text Notes 2800 4150 2    50   ~ 0
incoming power - castellation
Text Notes 2250 4950 2    50   ~ 0
pair 1 connector
Text Notes 2300 5450 2    50   ~ 0
pair 2 connector
$Comp
L Device:C C1
U 1 1 5F81D062
P 1800 4400
AR Path="/5F81D062" Ref="C1"  Part="1" 
AR Path="/5F808E80/5F81D062" Ref="C?"  Part="1" 
F 0 "C1" H 1915 4446 50  0000 L CNN
F 1 "10nF" H 1915 4355 50  0000 L CNN
F 2 "" H 1838 4250 50  0001 C CNN
F 3 "~" H 1800 4400 50  0001 C CNN
	1    1800 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 4250 1800 4250
Wire Wire Line
	2100 4350 2100 4550
Wire Wire Line
	1800 4550 2100 4550
Wire Wire Line
	1800 4550 1650 4550
Connection ~ 1800 4550
Wire Wire Line
	1800 4250 1650 4250
Connection ~ 1800 4250
$Comp
L power:GND #PWR0112
U 1 1 5F81D06F
P 1650 4550
AR Path="/5F81D06F" Ref="#PWR0112"  Part="1" 
AR Path="/5F808E80/5F81D06F" Ref="#PWR?"  Part="1" 
F 0 "#PWR0112" H 1650 4300 50  0001 C CNN
F 1 "GND" V 1655 4377 50  0000 C CNN
F 2 "" H 1650 4550 50  0001 C CNN
F 3 "" H 1650 4550 50  0001 C CNN
	1    1650 4550
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0113
U 1 1 5F81D075
P 2100 1050
AR Path="/5F81D075" Ref="#PWR0113"  Part="1" 
AR Path="/5F808E80/5F81D075" Ref="#PWR?"  Part="1" 
F 0 "#PWR0113" H 2100 900 50  0001 C CNN
F 1 "VCC" H 2115 1223 50  0000 C CNN
F 2 "" H 2100 1050 50  0001 C CNN
F 3 "" H 2100 1050 50  0001 C CNN
	1    2100 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 1050 2100 1300
Connection ~ 2100 1300
Wire Wire Line
	2100 1300 2100 2000
$Comp
L power:VCC #PWR0114
U 1 1 5F81D07E
P 1650 4250
AR Path="/5F81D07E" Ref="#PWR0114"  Part="1" 
AR Path="/5F808E80/5F81D07E" Ref="#PWR?"  Part="1" 
F 0 "#PWR0114" H 1650 4100 50  0001 C CNN
F 1 "VCC" V 1665 4377 50  0000 L CNN
F 2 "" H 1650 4250 50  0001 C CNN
F 3 "" H 1650 4250 50  0001 C CNN
	1    1650 4250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 5F81D084
P 2100 3600
AR Path="/5F81D084" Ref="#PWR0115"  Part="1" 
AR Path="/5F808E80/5F81D084" Ref="#PWR?"  Part="1" 
F 0 "#PWR0115" H 2100 3350 50  0001 C CNN
F 1 "GND" H 2105 3427 50  0000 C CNN
F 2 "" H 2100 3600 50  0001 C CNN
F 3 "" H 2100 3600 50  0001 C CNN
	1    2100 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 3200 2100 3600
$Comp
L BottleLights-rescue:PIC16F1574-ISL_0-MCU_Microchip_PIC16 U5
U 1 1 5F81D08B
P 2100 2600
AR Path="/5F81D08B" Ref="U5"  Part="1" 
AR Path="/5F808E80/5F81D08B" Ref="U?"  Part="1" 
F 0 "U5" H 2075 3378 50  0000 C CNN
F 1 "PIC16F1574-ISL_0" H 2075 3287 50  0000 C CNN
F 2 "" H 2100 2600 50  0001 C CNN
F 3 "" H 2100 2600 50  0001 C CNN
F 4 "X" H 2100 2600 50  0001 C CNN "Spice_Primitive"
F 5 "PIC16F1574" H 2100 2600 50  0001 C CNN "Spice_Model"
F 6 "N" H 2100 2600 50  0001 C CNN "Spice_Netlist_Enabled"
	1    2100 2600
	1    0    0    -1  
$EndComp
NoConn ~ 1250 2500
NoConn ~ 1250 2700
NoConn ~ 1250 2800
Wire Wire Line
	1150 2300 1250 2300
Wire Wire Line
	900  2600 900  1200
Wire Wire Line
	900  2600 1250 2600
Wire Wire Line
	1150 2300 1150 1500
Wire Wire Line
	1000 2400 1000 1600
Wire Wire Line
	1000 2400 1250 2400
NoConn ~ 2900 2300
NoConn ~ 2900 2400
NoConn ~ 2900 2500
NoConn ~ 2900 2600
NoConn ~ 2900 2700
NoConn ~ 2900 2800
Wire Wire Line
	900  1200 2450 1200
Wire Wire Line
	1000 1600 2450 1600
Wire Wire Line
	1150 1500 2450 1500
Wire Wire Line
	2100 1300 2450 1300
Text GLabel 8200 2100 1    50   Input ~ 0
pin6led
Text GLabel 8200 3300 1    50   Input ~ 0
pin7led
Text GLabel 8200 3800 3    50   Input ~ 0
pin8led
Text GLabel 8200 2600 3    50   Input ~ 0
pin9led
Wire Wire Line
	8200 3800 8200 3700
Connection ~ 8200 3700
Wire Wire Line
	8200 3700 8900 3700
Wire Wire Line
	8200 3300 8200 3400
Connection ~ 8200 3400
Wire Wire Line
	8200 3400 8900 3400
Wire Wire Line
	8200 2600 8200 2500
Connection ~ 8200 2500
Wire Wire Line
	8200 2500 8900 2500
Wire Wire Line
	8200 2100 8200 2200
Connection ~ 8200 2200
Wire Wire Line
	8200 2200 8900 2200
Wire Wire Line
	3650 1400 3650 1300
Connection ~ 3650 1400
Wire Wire Line
	4500 1400 4500 1500
Connection ~ 4500 1400
$Comp
L power:VCC #PWR0103
U 1 1 5F83B084
P 6600 2800
AR Path="/5F83B084" Ref="#PWR0103"  Part="1" 
AR Path="/5F808E80/5F83B084" Ref="#PWR?"  Part="1" 
F 0 "#PWR0103" H 6600 2650 50  0001 C CNN
F 1 "VCC" H 6615 2973 50  0000 C CNN
F 2 "" H 6600 2800 50  0001 C CNN
F 3 "" H 6600 2800 50  0001 C CNN
	1    6600 2800
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0105
U 1 1 5F83BA74
P 6600 3000
AR Path="/5F83BA74" Ref="#PWR0105"  Part="1" 
AR Path="/5F808E80/5F83BA74" Ref="#PWR?"  Part="1" 
F 0 "#PWR0105" H 6600 2850 50  0001 C CNN
F 1 "VCC" H 6615 3173 50  0000 C CNN
F 2 "" H 6600 3000 50  0001 C CNN
F 3 "" H 6600 3000 50  0001 C CNN
	1    6600 3000
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0106
U 1 1 5F83BC99
P 6600 3800
AR Path="/5F83BC99" Ref="#PWR0106"  Part="1" 
AR Path="/5F808E80/5F83BC99" Ref="#PWR?"  Part="1" 
F 0 "#PWR0106" H 6600 3650 50  0001 C CNN
F 1 "VCC" H 6615 3973 50  0000 C CNN
F 2 "" H 6600 3800 50  0001 C CNN
F 3 "" H 6600 3800 50  0001 C CNN
	1    6600 3800
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0107
U 1 1 5F83C099
P 6600 4000
AR Path="/5F83C099" Ref="#PWR0107"  Part="1" 
AR Path="/5F808E80/5F83C099" Ref="#PWR?"  Part="1" 
F 0 "#PWR0107" H 6600 3850 50  0001 C CNN
F 1 "VCC" H 6615 4173 50  0000 C CNN
F 2 "" H 6600 4000 50  0001 C CNN
F 3 "" H 6600 4000 50  0001 C CNN
	1    6600 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	6500 4000 6600 4000
Wire Wire Line
	6600 3800 6500 3800
Wire Wire Line
	6500 3000 6600 3000
Wire Wire Line
	6600 2800 6500 2800
$Comp
L Device:R R?
U 1 1 5F7DB860
P 3950 2900
AR Path="/5F7559D3/5F7DB860" Ref="R?"  Part="1" 
AR Path="/5F7DB860" Ref="R1"  Part="1" 
F 0 "R1" H 3880 2854 50  0000 R CNN
F 1 "10k" H 3880 2945 50  0000 R CNN
F 2 "" V 3880 2900 50  0001 C CNN
F 3 "~" H 3950 2900 50  0001 C CNN
	1    3950 2900
	0    1    1    0   
$EndComp
Wire Wire Line
	4250 1500 4250 1650
Wire Wire Line
	4250 1300 4500 1300
Wire Wire Line
	3650 1500 4050 1500
$Comp
L Device:R_Pack04 RN?
U 1 1 5F72562A
P 6150 1450
F 0 "RN?" H 6338 1496 50  0000 L CNN
F 1 "47R" H 6338 1405 50  0000 L CNN
F 2 "Resistor_SMD:R_Array_Convex_4x0603" V 6425 1450 50  0001 C CNN
F 3 "~" H 6150 1450 50  0001 C CNN
	1    6150 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Pack02 RN?
U 1 1 5F7279C2
P 7100 1450
F 0 "RN?" H 7188 1496 50  0000 L CNN
F 1 "10k" H 7188 1405 50  0000 L CNN
F 2 "Resistor_SMD:R_Array_Convex_2x0402" V 7275 1450 50  0001 C CNN
F 3 "~" H 7100 1450 50  0001 C CNN
	1    7100 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Pack02 RN?
U 1 1 5F728A57
P 7650 1450
F 0 "RN?" H 7738 1496 50  0000 L CNN
F 1 "100R" H 7738 1405 50  0000 L CNN
F 2 "Resistor_SMD:R_Array_Convex_2x0402" V 7825 1450 50  0001 C CNN
F 3 "~" H 7650 1450 50  0001 C CNN
	1    7650 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Pack04 RN?
U 1 1 5F72B81D
P 6150 900
F 0 "RN?" H 6338 946 50  0000 L CNN
F 1 "22k" H 6338 855 50  0000 L CNN
F 2 "Resistor_SMD:R_Array_Convex_4x0603" V 6425 900 50  0001 C CNN
F 3 "~" H 6150 900 50  0001 C CNN
	1    6150 900 
	1    0    0    -1  
$EndComp
$EndSCHEMATC
