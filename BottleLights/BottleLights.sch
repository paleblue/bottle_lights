EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Bottle Light Controller"
Date ""
Rev "2"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 3050 2400 0    50   ~ 0
pin9
Text Label 3050 2600 0    50   ~ 0
pin7
Text Label 3050 2500 0    50   ~ 0
pin8
Text Label 3050 2700 0    50   ~ 0
pin6
Text Label 3050 2800 0    50   ~ 0
pin5
$Comp
L power:GND #PWR?
U 1 1 5F7DB866
P 4400 5050
AR Path="/5F7559D3/5F7DB866" Ref="#PWR?"  Part="1" 
AR Path="/5F7DB866" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 4400 4800 50  0001 C CNN
F 1 "GND" H 4405 4877 50  0000 C CNN
F 2 "" H 4400 5050 50  0001 C CNN
F 3 "" H 4400 5050 50  0001 C CNN
	1    4400 5050
	0    1    1    0   
$EndComp
$Comp
L UMZ1NTR:UMZ1NTR U?
U 1 1 5F7DB87A
P 4400 3800
AR Path="/5F7559D3/5F7DB87A" Ref="U?"  Part="1" 
AR Path="/5F7DB87A" Ref="U2"  Part="1" 
F 0 "U2" H 5450 4187 60  0000 C CNN
F 1 "UMZ1NTR" H 5450 4081 60  0000 C CNN
F 2 "UMZ1NTR:SOP65P210X100-6N" H 5450 4040 60  0001 C CNN
F 3 "" H 4400 3800 60  0000 C CNN
F 4 "X" H 4400 3800 50  0001 C CNN "Spice_Primitive"
F 5 "UMZ1N" H 4400 3800 50  0001 C CNN "Spice_Model"
F 6 "Y" H 4400 3800 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "D:\\Projects\\_electronics\\bottle_lights\\umz1n.lib" H 4400 3800 50  0001 C CNN "Spice_Lib_File"
	1    4400 3800
	1    0    0    -1  
$EndComp
$Comp
L UMZ1NTR:UMZ1NTR U?
U 1 1 5F7DB884
P 4400 4850
AR Path="/5F7559D3/5F7DB884" Ref="U?"  Part="1" 
AR Path="/5F7DB884" Ref="U3"  Part="1" 
F 0 "U3" H 5450 5237 60  0000 C CNN
F 1 "UMZ1NTR" H 5450 5131 60  0000 C CNN
F 2 "UMZ1NTR:SOP65P210X100-6N" H 5450 5090 60  0001 C CNN
F 3 "" H 4400 4850 60  0000 C CNN
F 4 "X" H 4400 4850 50  0001 C CNN "Spice_Primitive"
F 5 "UMZ1N" H 4400 4850 50  0001 C CNN "Spice_Model"
F 6 "Y" H 4400 4850 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "D:\\Projects\\_electronics\\bottle_lights\\umz1n.lib" H 4400 4850 50  0001 C CNN "Spice_Lib_File"
	1    4400 4850
	1    0    0    -1  
$EndComp
$Comp
L UMZ1NTR:UMZ1NTR U?
U 1 1 5F7DB88E
P 4400 5850
AR Path="/5F7559D3/5F7DB88E" Ref="U?"  Part="1" 
AR Path="/5F7DB88E" Ref="U4"  Part="1" 
F 0 "U4" H 5450 6237 60  0000 C CNN
F 1 "UMZ1NTR" H 5450 6131 60  0000 C CNN
F 2 "UMZ1NTR:SOP65P210X100-6N" H 5450 6090 60  0001 C CNN
F 3 "" H 4400 5850 60  0000 C CNN
F 4 "X" H 4400 5850 50  0001 C CNN "Spice_Primitive"
F 5 "UMZ1N" H 4400 5850 50  0001 C CNN "Spice_Model"
F 6 "Y" H 4400 5850 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "D:\\Projects\\_electronics\\bottle_lights\\umz1n.lib" H 4400 5850 50  0001 C CNN "Spice_Lib_File"
	1    4400 5850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F7DB8D6
P 4400 6050
AR Path="/5F7559D3/5F7DB8D6" Ref="#PWR?"  Part="1" 
AR Path="/5F7DB8D6" Ref="#PWR0108"  Part="1" 
F 0 "#PWR0108" H 4400 5800 50  0001 C CNN
F 1 "GND" H 4405 5877 50  0000 C CNN
F 2 "" H 4400 6050 50  0001 C CNN
F 3 "" H 4400 6050 50  0001 C CNN
	1    4400 6050
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F7DB8DC
P 4400 5850
AR Path="/5F7559D3/5F7DB8DC" Ref="#PWR?"  Part="1" 
AR Path="/5F7DB8DC" Ref="#PWR0109"  Part="1" 
F 0 "#PWR0109" H 4400 5600 50  0001 C CNN
F 1 "GND" H 4405 5677 50  0000 C CNN
F 2 "" H 4400 5850 50  0001 C CNN
F 3 "" H 4400 5850 50  0001 C CNN
	1    4400 5850
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F7DB8E2
P 4400 4850
AR Path="/5F7559D3/5F7DB8E2" Ref="#PWR?"  Part="1" 
AR Path="/5F7DB8E2" Ref="#PWR0110"  Part="1" 
F 0 "#PWR0110" H 4400 4600 50  0001 C CNN
F 1 "GND" H 4405 4677 50  0000 C CNN
F 2 "" H 4400 4850 50  0001 C CNN
F 3 "" H 4400 4850 50  0001 C CNN
	1    4400 4850
	0    1    1    0   
$EndComp
Text Label 3700 5950 2    50   ~ 0
pin5
Wire Wire Line
	4100 4950 4400 4950
Wire Wire Line
	6500 5950 6700 5950
Text Label 7350 5500 0    50   ~ 0
pin5
Text Label 7350 5400 0    50   ~ 0
pin5
Wire Wire Line
	6500 3900 6800 3900
Wire Wire Line
	6500 2900 6800 2900
Text Label 3200 3300 2    50   ~ 0
pin6
Text Label 3200 3400 2    50   ~ 0
pin7
Text Label 7150 3900 0    50   ~ 0
pin8
Text Label 7150 2900 0    50   ~ 0
pin9
Wire Wire Line
	8000 6050 6500 6050
Wire Wire Line
	7400 4850 6500 4850
Wire Wire Line
	7850 3450 7850 5850
Wire Wire Line
	7850 5850 6500 5850
Connection ~ 7850 3450
Text Label 3700 4950 2    50   ~ 0
pin5
$Comp
L power:GND #PWR0111
U 1 1 5F81D047
P 2450 1400
AR Path="/5F81D047" Ref="#PWR0111"  Part="1" 
AR Path="/5F808E80/5F81D047" Ref="#PWR?"  Part="1" 
F 0 "#PWR0111" H 2450 1150 50  0001 C CNN
F 1 "GND" V 2455 1227 50  0000 C CNN
F 2 "" H 2450 1400 50  0001 C CNN
F 3 "" H 2450 1400 50  0001 C CNN
	1    2450 1400
	0    1    1    0   
$EndComp
Text Notes 2800 4150 2    50   ~ 0
incoming power - castellation
$Comp
L Device:C C1
U 1 1 5F81D062
P 1800 4400
AR Path="/5F81D062" Ref="C1"  Part="1" 
AR Path="/5F808E80/5F81D062" Ref="C?"  Part="1" 
F 0 "C1" H 1915 4446 50  0000 L CNN
F 1 "10nF" H 1915 4355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1838 4250 50  0001 C CNN
F 3 "~" H 1800 4400 50  0001 C CNN
	1    1800 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 4250 1800 4250
Wire Wire Line
	1800 4550 2100 4550
Wire Wire Line
	1800 4550 1650 4550
Connection ~ 1800 4550
Wire Wire Line
	1800 4250 1650 4250
Connection ~ 1800 4250
$Comp
L power:GND #PWR0112
U 1 1 5F81D06F
P 1650 4550
AR Path="/5F81D06F" Ref="#PWR0112"  Part="1" 
AR Path="/5F808E80/5F81D06F" Ref="#PWR?"  Part="1" 
F 0 "#PWR0112" H 1650 4300 50  0001 C CNN
F 1 "GND" V 1655 4377 50  0000 C CNN
F 2 "" H 1650 4550 50  0001 C CNN
F 3 "" H 1650 4550 50  0001 C CNN
	1    1650 4550
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0113
U 1 1 5F81D075
P 2100 1050
AR Path="/5F81D075" Ref="#PWR0113"  Part="1" 
AR Path="/5F808E80/5F81D075" Ref="#PWR?"  Part="1" 
F 0 "#PWR0113" H 2100 900 50  0001 C CNN
F 1 "VCC" H 2115 1223 50  0000 C CNN
F 2 "" H 2100 1050 50  0001 C CNN
F 3 "" H 2100 1050 50  0001 C CNN
	1    2100 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 1050 2100 1300
Connection ~ 2100 1300
Wire Wire Line
	2100 1300 2100 2000
$Comp
L power:VCC #PWR0114
U 1 1 5F81D07E
P 1650 4250
AR Path="/5F81D07E" Ref="#PWR0114"  Part="1" 
AR Path="/5F808E80/5F81D07E" Ref="#PWR?"  Part="1" 
F 0 "#PWR0114" H 1650 4100 50  0001 C CNN
F 1 "VCC" V 1665 4377 50  0000 L CNN
F 2 "" H 1650 4250 50  0001 C CNN
F 3 "" H 1650 4250 50  0001 C CNN
	1    1650 4250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0115
U 1 1 5F81D084
P 2100 3600
AR Path="/5F81D084" Ref="#PWR0115"  Part="1" 
AR Path="/5F808E80/5F81D084" Ref="#PWR?"  Part="1" 
F 0 "#PWR0115" H 2100 3350 50  0001 C CNN
F 1 "GND" H 2105 3427 50  0000 C CNN
F 2 "" H 2100 3600 50  0001 C CNN
F 3 "" H 2100 3600 50  0001 C CNN
	1    2100 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 3200 2100 3600
$Comp
L BottleLights-rescue:PIC16F1574-ISL_0-MCU_Microchip_PIC16 U5
U 1 1 5F81D08B
P 2100 2600
AR Path="/5F81D08B" Ref="U5"  Part="1" 
AR Path="/5F808E80/5F81D08B" Ref="U?"  Part="1" 
F 0 "U5" H 1500 3100 50  0000 C CNN
F 1 "PIC16F1574-ISL_0" H 2500 3100 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 2100 2600 50  0001 C CNN
F 3 "" H 2100 2600 50  0001 C CNN
F 4 "X" H 2100 2600 50  0001 C CNN "Spice_Primitive"
F 5 "PIC16F1574" H 2100 2600 50  0001 C CNN "Spice_Model"
F 6 "N" H 2100 2600 50  0001 C CNN "Spice_Netlist_Enabled"
	1    2100 2600
	1    0    0    -1  
$EndComp
NoConn ~ 1250 2500
NoConn ~ 1250 2700
NoConn ~ 1250 2800
Wire Wire Line
	1150 2300 1250 2300
Wire Wire Line
	900  2600 900  1200
Wire Wire Line
	900  2600 1250 2600
Wire Wire Line
	1150 2300 1150 1500
Wire Wire Line
	1000 2400 1000 1600
Wire Wire Line
	1000 2400 1250 2400
NoConn ~ 2900 2300
Wire Wire Line
	900  1200 2550 1200
Wire Wire Line
	1000 1600 2550 1600
Wire Wire Line
	1150 1500 2550 1500
Wire Wire Line
	2100 1300 2550 1300
$Comp
L power:VCC #PWR0103
U 1 1 5F83B084
P 6600 2800
AR Path="/5F83B084" Ref="#PWR0103"  Part="1" 
AR Path="/5F808E80/5F83B084" Ref="#PWR?"  Part="1" 
F 0 "#PWR0103" H 6600 2650 50  0001 C CNN
F 1 "VCC" H 6615 2973 50  0000 C CNN
F 2 "" H 6600 2800 50  0001 C CNN
F 3 "" H 6600 2800 50  0001 C CNN
	1    6600 2800
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0105
U 1 1 5F83BA74
P 6600 3000
AR Path="/5F83BA74" Ref="#PWR0105"  Part="1" 
AR Path="/5F808E80/5F83BA74" Ref="#PWR?"  Part="1" 
F 0 "#PWR0105" H 6600 2850 50  0001 C CNN
F 1 "VCC" H 6615 3173 50  0000 C CNN
F 2 "" H 6600 3000 50  0001 C CNN
F 3 "" H 6600 3000 50  0001 C CNN
	1    6600 3000
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0106
U 1 1 5F83BC99
P 6600 3800
AR Path="/5F83BC99" Ref="#PWR0106"  Part="1" 
AR Path="/5F808E80/5F83BC99" Ref="#PWR?"  Part="1" 
F 0 "#PWR0106" H 6600 3650 50  0001 C CNN
F 1 "VCC" H 6615 3973 50  0000 C CNN
F 2 "" H 6600 3800 50  0001 C CNN
F 3 "" H 6600 3800 50  0001 C CNN
	1    6600 3800
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0107
U 1 1 5F83C099
P 6600 4000
AR Path="/5F83C099" Ref="#PWR0107"  Part="1" 
AR Path="/5F808E80/5F83C099" Ref="#PWR?"  Part="1" 
F 0 "#PWR0107" H 6600 3850 50  0001 C CNN
F 1 "VCC" H 6615 4173 50  0000 C CNN
F 2 "" H 6600 4000 50  0001 C CNN
F 3 "" H 6600 4000 50  0001 C CNN
	1    6600 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	6500 4000 6600 4000
Wire Wire Line
	6600 3800 6500 3800
Wire Wire Line
	6500 3000 6600 3000
Wire Wire Line
	6600 2800 6500 2800
Wire Wire Line
	2900 2400 3050 2400
Wire Wire Line
	2900 2500 3050 2500
Wire Wire Line
	2900 2600 3050 2600
Wire Wire Line
	2900 2700 3050 2700
Wire Wire Line
	2900 2800 3050 2800
Wire Wire Line
	2450 1400 2550 1400
$Comp
L Connector:TestPoint TP1
U 1 1 5F759F03
P 2550 1200
F 0 "TP1" V 2550 1400 50  0000 L CNN
F 1 "MCLR" V 2550 1550 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 2750 1200 50  0001 C CNN
F 3 "~" H 2750 1200 50  0001 C CNN
	1    2550 1200
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 5F75B2C7
P 2550 1300
F 0 "TP2" V 2550 1500 50  0000 L CNN
F 1 "VCC" V 2550 1650 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 2750 1300 50  0001 C CNN
F 3 "~" H 2750 1300 50  0001 C CNN
	1    2550 1300
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP3
U 1 1 5F75B4E1
P 2550 1400
F 0 "TP3" V 2550 1600 50  0000 L CNN
F 1 "GND" V 2550 1750 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 2750 1400 50  0001 C CNN
F 3 "~" H 2750 1400 50  0001 C CNN
	1    2550 1400
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP4
U 1 1 5F75B6F6
P 2550 1500
F 0 "TP4" V 2550 1700 50  0000 L CNN
F 1 "DAT" V 2550 1850 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 2750 1500 50  0001 C CNN
F 3 "~" H 2750 1500 50  0001 C CNN
	1    2550 1500
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP5
U 1 1 5F75B8E7
P 2550 1600
F 0 "TP5" V 2550 1800 50  0000 L CNN
F 1 "CLK" V 2550 1950 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 2750 1600 50  0001 C CNN
F 3 "~" H 2750 1600 50  0001 C CNN
	1    2550 1600
	0    1    1    0   
$EndComp
Wire Wire Line
	7400 2400 9250 2400
Wire Wire Line
	7550 2500 9250 2500
Wire Wire Line
	7850 3450 9250 3450
Wire Wire Line
	8000 3550 9250 3550
$Comp
L Connector:TestPoint TP6
U 1 1 5F7617D7
P 9250 2400
F 0 "TP6" V 9250 2600 50  0000 L CNN
F 1 "69_pair_A" V 9250 2750 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.5x4mm" H 9450 2400 50  0001 C CNN
F 3 "~" H 9450 2400 50  0001 C CNN
	1    9250 2400
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP7
U 1 1 5F762519
P 9250 2500
F 0 "TP7" V 9250 2700 50  0000 L CNN
F 1 "69_pair_B" V 9250 2850 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.5x4mm" H 9450 2500 50  0001 C CNN
F 3 "~" H 9450 2500 50  0001 C CNN
	1    9250 2500
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP8
U 1 1 5F764737
P 9250 3450
F 0 "TP8" V 9250 3650 50  0000 L CNN
F 1 "78_pair_A" V 9250 3800 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.5x4mm" H 9450 3450 50  0001 C CNN
F 3 "~" H 9450 3450 50  0001 C CNN
	1    9250 3450
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP9
U 1 1 5F764D64
P 9250 3550
F 0 "TP9" V 9250 3750 50  0000 L CNN
F 1 "78_pair_B" V 9250 3900 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_1.5x4mm" H 9450 3550 50  0001 C CNN
F 3 "~" H 9450 3550 50  0001 C CNN
	1    9250 3550
	0    1    1    0   
$EndComp
Wire Wire Line
	7250 5500 7350 5500
Wire Wire Line
	7050 5400 7350 5400
Wire Wire Line
	7550 5050 6500 5050
Wire Wire Line
	6500 4950 6700 4950
Wire Wire Line
	4100 5950 4400 5950
Wire Wire Line
	3200 3300 3550 3300
Wire Wire Line
	3200 3400 3350 3400
Wire Wire Line
	3900 2900 4400 2900
Wire Wire Line
	3900 3900 4400 3900
Connection ~ 8000 3550
Wire Wire Line
	8000 3550 8000 6050
Wire Wire Line
	4400 3000 4100 3000
Wire Wire Line
	4400 2800 4000 2800
Wire Wire Line
	4000 2800 4000 3350
Wire Wire Line
	4100 4000 4400 4000
$Comp
L UMZ1NTR:UMZ1NTR U?
U 1 1 5F7DB870
P 4400 2800
AR Path="/5F7559D3/5F7DB870" Ref="U?"  Part="1" 
AR Path="/5F7DB870" Ref="U1"  Part="1" 
F 0 "U1" H 5450 3187 60  0000 C CNN
F 1 "UMZ1NTR" H 5450 3081 60  0000 C CNN
F 2 "UMZ1NTR:SOP65P210X100-6N" H 5450 3040 60  0001 C CNN
F 3 "" H 4400 2800 60  0000 C CNN
F 4 "X" H 4400 2800 50  0001 C CNN "Spice_Primitive"
F 5 "UMZ1N" H 4400 2800 50  0001 C CNN "Spice_Model"
F 6 "Y" H 4400 2800 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "D:\\Projects\\_electronics\\bottle_lights\\umz1n.lib" H 4400 2800 50  0001 C CNN "Spice_Lib_File"
	1    4400 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 2500 7550 3250
Wire Wire Line
	7400 2400 7400 3350
Connection ~ 7400 3350
Wire Wire Line
	7400 3350 7400 4850
Connection ~ 7550 3250
Wire Wire Line
	7550 3250 7550 5050
Text Notes 4350 2800 2    31   ~ 0
69_pair_A
Text Notes 4350 3000 2    31   ~ 0
69_pair_B
Wire Wire Line
	4000 3800 4000 3450
Wire Wire Line
	4000 3800 4400 3800
Text Notes 4350 4000 2    31   ~ 0
78_pair_B
Text Notes 4350 3800 2    31   ~ 0
78_pair_A
Wire Wire Line
	6700 5950 6700 5500
Wire Wire Line
	6700 5500 6950 5500
Wire Wire Line
	6700 4950 6700 5400
Wire Wire Line
	6700 5400 6750 5400
Wire Wire Line
	3900 3900 3900 3400
Wire Wire Line
	3900 3400 3650 3400
Wire Wire Line
	3900 2900 3900 3300
Wire Wire Line
	3900 3300 3850 3300
Wire Wire Line
	4100 4000 4100 3550
Wire Wire Line
	4100 3000 4100 3250
$Comp
L Device:R R10
U 1 1 5F758F8E
P 6950 2900
F 0 "R10" V 6743 2900 50  0000 C CNN
F 1 "22k" V 6834 2900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6880 2900 50  0001 C CNN
F 3 "~" H 6950 2900 50  0001 C CNN
	1    6950 2900
	0    1    1    0   
$EndComp
Wire Wire Line
	7150 2900 7100 2900
$Comp
L Device:R R11
U 1 1 5F765128
P 6950 3900
F 0 "R11" V 6743 3900 50  0000 C CNN
F 1 "22k" V 6834 3900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6880 3900 50  0001 C CNN
F 3 "~" H 6950 3900 50  0001 C CNN
	1    6950 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	7150 3900 7100 3900
$Comp
L Device:R R4
U 1 1 5F76E138
P 3950 5950
F 0 "R4" V 3743 5950 50  0000 C CNN
F 1 "22k" V 3834 5950 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3880 5950 50  0001 C CNN
F 3 "~" H 3950 5950 50  0001 C CNN
	1    3950 5950
	0    1    1    0   
$EndComp
Wire Wire Line
	3700 5950 3800 5950
$Comp
L Device:R R3
U 1 1 5F7716CA
P 3950 4950
F 0 "R3" V 3743 4950 50  0000 C CNN
F 1 "22k" V 3834 4950 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3880 4950 50  0001 C CNN
F 3 "~" H 3950 4950 50  0001 C CNN
	1    3950 4950
	0    1    1    0   
$EndComp
Wire Wire Line
	3700 4950 3800 4950
$Comp
L Device:R R2
U 1 1 5F787BCF
P 3700 3300
F 0 "R2" V 3493 3300 50  0000 C CNN
F 1 "10k" V 3584 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3630 3300 50  0001 C CNN
F 3 "~" H 3700 3300 50  0001 C CNN
	1    3700 3300
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5F788024
P 3500 3400
F 0 "R1" V 3293 3400 50  0000 C CNN
F 1 "10k" V 3384 3400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3430 3400 50  0001 C CNN
F 3 "~" H 3500 3400 50  0001 C CNN
	1    3500 3400
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 5F790DAB
P 6900 5400
F 0 "R9" V 6693 5400 50  0000 C CNN
F 1 "100R" V 6784 5400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6830 5400 50  0001 C CNN
F 3 "~" H 6900 5400 50  0001 C CNN
	1    6900 5400
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 5F79142B
P 7100 5500
F 0 "R12" V 6893 5500 50  0000 C CNN
F 1 "100R" V 6984 5500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7030 5500 50  0001 C CNN
F 3 "~" H 7100 5500 50  0001 C CNN
	1    7100 5500
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP10
U 1 1 5F81C675
P 2100 4250
F 0 "TP10" V 2100 4450 50  0000 L CNN
F 1 "VCC" V 2100 4700 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_2.0x2.0mm_Drill1.0mm_nocrtyd" H 2300 4250 50  0001 C CNN
F 3 "~" H 2300 4250 50  0001 C CNN
	1    2100 4250
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP11
U 1 1 5F81CCDE
P 2100 4550
F 0 "TP11" V 2100 4750 50  0000 L CNN
F 1 "GND" V 2100 5000 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_D2.0mm_Drill1.0mm_nocrtyd" H 2300 4550 50  0001 C CNN
F 3 "~" H 2300 4550 50  0001 C CNN
	1    2100 4550
	0    1    1    0   
$EndComp
Wire Wire Line
	5000 3550 8000 3550
Wire Wire Line
	4000 3450 4500 3450
Wire Wire Line
	4000 3350 4300 3350
Wire Wire Line
	4100 3550 4700 3550
Wire Wire Line
	4400 3250 7550 3250
Wire Wire Line
	4600 3350 7400 3350
Wire Wire Line
	4800 3450 7850 3450
$Comp
L Device:R R8
U 1 1 5F777FB8
P 4850 3550
F 0 "R8" V 4643 3550 50  0000 C CNN
F 1 "47" V 4734 3550 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4780 3550 50  0001 C CNN
F 3 "~" H 4850 3550 50  0001 C CNN
	1    4850 3550
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 5F777E94
P 4650 3450
F 0 "R7" V 4443 3450 50  0000 C CNN
F 1 "47" V 4534 3450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4580 3450 50  0001 C CNN
F 3 "~" H 4650 3450 50  0001 C CNN
	1    4650 3450
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 5F777CCC
P 4450 3350
F 0 "R6" V 4243 3350 50  0000 C CNN
F 1 "47" V 4334 3350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4380 3350 50  0001 C CNN
F 3 "~" H 4450 3350 50  0001 C CNN
	1    4450 3350
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 5F7773F9
P 4250 3250
F 0 "R5" V 4043 3250 50  0000 C CNN
F 1 "47" V 4134 3250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4180 3250 50  0001 C CNN
F 3 "~" H 4250 3250 50  0001 C CNN
	1    4250 3250
	0    1    1    0   
$EndComp
$EndSCHEMATC
