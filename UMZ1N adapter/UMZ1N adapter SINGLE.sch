EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L UMZ1NTR:UMZ1NTR U1
U 1 1 5F872FBD
P 3600 2000
F 0 "U1" H 4650 2387 60  0000 C CNN
F 1 "UMZ1NTR" H 4650 2281 60  0000 C CNN
F 2 "UMZ1NTR:SOP65P210X100-6N" H 4650 2240 60  0001 C CNN
F 3 "" H 3600 2000 60  0000 C CNN
	1    3600 2000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J2
U 1 1 5F6E4EA5
P 6050 2100
F 0 "J2" H 6022 2032 50  0000 R CNN
F 1 "Conn_01x03_Male" H 6022 2123 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6050 2100 50  0001 C CNN
F 3 "~" H 6050 2100 50  0001 C CNN
	1    6050 2100
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J1
U 1 1 5F6E5B86
P 3250 2100
F 0 "J1" H 3358 2381 50  0000 C CNN
F 1 "Conn_01x03_Male" H 3358 2290 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3250 2100 50  0001 C CNN
F 3 "~" H 3250 2100 50  0001 C CNN
	1    3250 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 2000 5850 2000
Wire Wire Line
	5700 2100 5850 2100
Wire Wire Line
	5700 2200 5850 2200
Wire Wire Line
	3600 2000 3450 2000
Wire Wire Line
	3450 2100 3600 2100
Wire Wire Line
	3600 2200 3450 2200
$EndSCHEMATC
